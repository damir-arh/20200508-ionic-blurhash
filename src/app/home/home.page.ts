import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  public imageSrc: string;

  constructor() {}
  
  ngOnInit(): void {
    // artificially delay image loading to show blurhash for longer time
    setTimeout(() => {
      this.imageSrc = 'assets/img/photo.jpg';
    }, 1000);
  }

}
