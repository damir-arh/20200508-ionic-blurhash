import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BlurhashComponent } from './blurhash.component';

describe('BlurhashComponent', () => {
  let component: BlurhashComponent;
  let fixture: ComponentFixture<BlurhashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlurhashComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BlurhashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
