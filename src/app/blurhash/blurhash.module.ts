import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BlurhashComponent } from './blurhash.component';

@NgModule({
  imports: [ CommonModule, FormsModule, IonicModule,],
  declarations: [BlurhashComponent],
  exports: [BlurhashComponent]
})
export class BlurhashComponentModule {}
